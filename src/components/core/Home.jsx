import React from "react";
import Users from '../user/Users';

const Home = () => (
  <div>
    <div className="jumbotron">
      <h2>Hi everyone!</h2>
      <p className="lead">Разработчики, Работодатели & Рекрутеры</p>
    </div>
    <Users />
    <div className="container">
    </div>
  </div>
);

export default Home;
