import React, { Component } from "react";
import { list } from "../../services/apiPost";
import DefaultPost from "../../images/mountains.jpg";
import { Link } from "react-router-dom";
const ReactSafeHtml = require('react-safe-html');

class Posts extends Component {
  constructor() {
    super();
    this.state = {
      posts: []
    };
  }

  loadPosts = () => {
    list().then(data => {
      if (data.error) {
        console.log(data.error);
      } else {
        this.setState({ posts: data });
      }
    });
  };

  componentDidMount() {
    this.loadPosts();
  }

  renderPosts = posts => {
    return (
      <div className="row">
        {posts.map((post, i) => {
          const posterId = post.postedBy
            ? `/user/${post.postedBy._id}`
            : "";
          const posterName = post.postedBy
            ? post.postedBy.name
            : " Unknown";

          return (
            <div className="card col-md-12" key={post._id}>
              <div className="card-body">
                <img
                  src={`${
                    process.env.REACT_APP_API_URL
                    }/post/photo/${post._id}`}
                  alt={post.title}
                  onError={img =>
                    (img.target.src = `${DefaultPost}`)
                  }
                  className="img-thunbnail mb-3"
                  style={{ width: "100%" }}
                />
                <h5 className="card-title">{post.title}</h5>
                <div className="card-text">
                  <ReactSafeHtml html={post.body.substring(0, 1000)} />
                </div>
                <br />
                <p className="font-italic mark">
                  Опубликовано{" "}
                  <Link to={`${posterId}`}>
                    {posterName}{" "}
                  </Link>
                  {(new Date(post.createdAt)).toLocaleString("ru", { day: 'numeric', month: 'long', year: 'numeric' })}
                </p>
                <Link
                  to={`/post/${post._id}`}
                  className="btn btn-block btn-outline-primary btn-sm"
                >
                  Read more
                </Link>
              </div>
            </div>
          );
        })}
      </div>
    );
  };

  render() {
    const { posts } = this.state;
    return (
      <div className="container">
        <h2 className="mt-5 mb-5">
          {!posts.length ? "Не опубликовано!" : "Публикации"}
        </h2>

        {this.renderPosts(posts)}
      </div>
    );
  }
}

export default Posts;
