import React, { Component } from "react";
import { singlePost, remove } from "../../services/apiPost";
import DefaultPost from "../../images/mountains.jpg";
import { Link, Redirect } from "react-router-dom";
import { isAuthenticated } from "../../auth";
const ReactSafeHtml = require('react-safe-html');

class SinglePost extends Component {
  state = {
    post: "",
    redirectToHome: false,
    redirectToSignin: false,
    like: false
  };

  componentDidMount = () => {
    const postId = this.props.match.params.postId;
    singlePost(postId).then(data => {
      if (data.error) {
        console.log(data.error);
      } else {
        this.setState({
          post: data
        });
      }
    });
  };

  updateComments = comments => {
    this.setState({ comments });
  };

  deletePost = () => {
    const postId = this.props.match.params.postId;
    const token = isAuthenticated().token;
    remove(postId, token).then(data => {
      if (data.error) {
        console.log(data.error);
      } else {
        this.setState({ redirectToHome: true });
      }
    });
  };

  deleteConfirmed = () => {
    let answer = window.confirm(
      "Вы действительно хотите удалить статью?"
    );
    if (answer) {
      this.deletePost();
    }
  };

  renderPost = post => {
    const posterId = post.postedBy ? `/user/${post.postedBy._id}` : "";
    const posterName = post.postedBy ? post.postedBy.name : " Unknown";

    return (
      <div className="card-body">
        <img
          src={`${process.env.REACT_APP_API_URL}/post/photo/${
            post._id
            }`}
          alt={post.title}
          onError={i => (i.target.src = `${DefaultPost}`)}
          className="img-thunbnail mb-3"
          style={{
            width: "100%",
            objectFit: "cover"
          }}
        />

        <div className="card-text"><ReactSafeHtml html={post.body} /></div>
        <br />
        <p className="font-italic mark">
          Опубликовано <Link to={`${posterId}`}>{posterName} </Link>
          {(new Date(post.createdAt)).toLocaleString("ru", { day: 'numeric', month: 'long', year: 'numeric' })}
        </p>
        <div className="d-block">
          <Link
            to={`/post/byuser/${post.postedBy._id}`}
            className="btn btn-block btn-outline-primary btn-sm mr-5"
          >
            Все публикации
          </Link>

          {isAuthenticated().user &&
            isAuthenticated().user._id === post.postedBy._id && (
              <>
                <Link
                  to={`/post/edit/${post._id}`}
                  className="btn btn-block btn-outline-warning btn-sm mr-5"
                >
                  Редактировать
                </Link>
                <button
                  onClick={this.deleteConfirmed}
                  className="btn btn-block btn-outline-danger btn-sm"
                >
                  Удалить статью
                </button>
              </>
            )}

          <div>
            {isAuthenticated().user &&
              isAuthenticated().user.role === "admin" && (
                <div class="card mt-5">
                  <div className="card-body">
                    <h5 className="card-title">Admin</h5>
                    <p className="mb-2 text-danger">
                      Edit/Delete as an Admin
                    </p>
                    <Link
                      to={`/post/edit/${post._id}`}
                      className="btn btn-block btn-outline-warning btn-sm"
                    >
                      Update Post
                    </Link>
                    <button
                      onClick={this.deleteConfirmed}
                      className="btn btn-block btn-outline-danger btn-sm"
                    >
                      Delete Post
                    </button>
                  </div>
                </div>
              )}
          </div>
        </div>
      </div>
    );
  };

  render() {
    const { post, redirectToHome, redirectToSignin } = this.state;

    if (redirectToHome) {
      return <Redirect to={`/user/${this.state.post.postedBy._id}`} />;
    } else if (redirectToSignin) {
      return <Redirect to={`/signin`} />;
    }

    return (
      <div className="container">
        <h2 className="display-2 mt-5 mb-5">{post.title}</h2>

        {!post ? (
          <div className="jumbotron text-center">
            <h2>Loading...</h2>
          </div>
        ) : (
            this.renderPost(post)
          )}
      </div>
    );
  }
}

export default SinglePost;
