import React, { Component } from "react";
import { freeListByUser, remove } from "../../services/apiPost";
import DefaultPost from "../../images/mountains.jpg";
import { isAuthenticated } from "../../auth";
import { Link, Redirect } from "react-router-dom";
const ReactSafeHtml = require('react-safe-html');

class Posts extends Component {
  constructor() {
    super();
    this.state = {
      redirectToHome: false,
      userId: null,
      posts: []
    };
  }

  loadPosts = (userId) => {
    freeListByUser(userId).then(data => {
      if (data.error) {
        console.log(data.error);
      } else {
        this.setState({ posts: data });
      }
    });
  };

  componentDidMount() {
    const userId = this.props.match.params.userId;
    this.setState({ userId })
    this.loadPosts(userId);
  }

  componentDidUpdate(prevProps) {
    if (this.state.posts == prevProps.posts) {
      const userId = this.props.match.params.userId;
      this.setState({ userId })
      this.loadPosts(userId);
    }
  }

  deletePost = (postId) => {
    const token = isAuthenticated().token;
    remove(postId, token).then(data => {
      if (data.error) {
        console.log(data.error);
      } else {
        this.setState({ redirectToHome: true });
      }
    });
  };

  deleteConfirmed = (postId) => {
    let answer = window.confirm(
      "Вы действительно хотите удалить статью?"
    );
    if (answer) {
      this.deletePost(postId);
    }
  };

  renderPosts = posts => {
    return (
      <div className="row">
        {posts.map((post) => {
          return (
            <div className="card col-md-12" key={post._id}>
              <div className="card-body">
                <img
                  src={`${
                    process.env.REACT_APP_API_URL
                    }/post/photo/${post._id}`}
                  alt={post.title}
                  onError={img =>
                    (img.target.src = `${DefaultPost}`)
                  }
                  className="img-card mb-3"
                  style={{ minWidth: "200px", maxWidth: "900px" }}
                />
                <h5 className="card-title">{post.title}</h5>
                <div className="card-text">
                  {post.body && <ReactSafeHtml html={post.body.substring(0, 1000)} />}
                </div>

                <br />

                <Link
                  to={`/post/${post._id}`}
                  className="btn btn-block btn-outline-primary btn-sm"
                >
                  Подробнее
                </Link>

                {isAuthenticated().user &&
                  isAuthenticated().user._id === post.postedBy._id && (
                    <>
                      <Link
                        to={`/post/edit/${post._id}`}
                      className="btn btn-block btn-outline-warning btn-sm"
                      >
                        Редактировать
                                </Link>
                      <button
                      onClick={() => this.deleteConfirmed(post._id)}
                      className="btn btn-block btn-outline-danger btn-sm"
                      >
                        Удалить
                      </button>
                    </>
                  )}

              </div>
              <div style={ { height: '20px', backgroundColor:'gray' } }></div>
            </div>
          );
        })}
      </div>
    );
  };

  render() {
    if (this.state.redirectToHome) {
      return <Redirect to={`/user/${this.state.userId}`} />
    }
    const { posts } = this.state;
    return (
      <div className="container">
        {this.renderPosts(posts)}
      </div>
    );
  }
}

export default Posts;
