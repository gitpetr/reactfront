import React, { Component } from "react";
import { singlePost, update } from "../../services/apiPost";
import { isAuthenticated } from "../../auth";
import { Redirect } from "react-router-dom";
import DefaultPost from "../../images/mountains.jpg";
import 'jodit';
import 'jodit/build/jodit.min.css';
import JoditEditor from "jodit-react";

class EditPost extends Component {
  constructor() {
    super();
    this.state = {
      id: "",
      post_id: "",
      title: "",
      body: "",
      redirectToProfile: false,
      error: "",
      fileSize: 0,
      loading: false
    };
  }

  init = postId => {
    singlePost(postId).then(data => {
      if (data.error) {
        this.setState({ redirectToProfile: true });
      } else {
        this.setState({
          id: data.postedBy._id,
          post_id: data._id,
          title: data.title,
          body: data.body,
          error: ""
        });
      }
    });
  };

  componentDidMount() {
    this.postData = new FormData();
    const postId = this.props.match.params.postId;
    this.init(postId);
  }

  jodit;
  setRef = jodit => this.jodit = jodit;

  config = {
    readonly: false
  }

  isValid = () => {
    const { title, body, fileSize } = this.state;
    if (fileSize > 1000000) {
      this.setState({
        error: "Картинка должна быть меньше 1000kb",
        loading: false
      });
      return false;
    }
    if (title.length === 0 || body.length === 0) {
      this.setState({ error: "Все поля должны быть заполнены", loading: false });
      return false;
    }
    return true;
  };

  handleChange = name => event => {
    this.setState({ error: "" });
    const value =
      name === "photo" ? event.target.files[0] : event.target.value;

    const fileSize = name === "photo" ? event.target.files[0].size : 0;
    this.postData.set(name, value);
    this.setState({ [name]: value, fileSize });
  };

  updateContent = (value) => {
    this.postData.set("body", value);
    this.setState({ body: value })
  }

  clickSubmit = event => {
    event.preventDefault();
    this.setState({ loading: true });

    if (this.isValid()) {
      const postId = this.props.match.params.postId;
      const token = isAuthenticated().token;

      update(postId, token, this.postData).then(data => {
        if (data.error) this.setState({ error: data.error });
        else {
          this.setState({
            loading: false,
            title: "",
            body: "",
            redirectToProfile: true
          });
        }
      });
    }
  };

  editPostForm = (title, body) => (
    <form>
      <div className="form-group">
        <label className="text-muted mt-3 mb-0">Загрузите картинку</label>
        <input
          onChange={this.handleChange("photo")}
          type="file"
          accept="image/*"
          className="form-control mt-0"
        />
      </div>
      <div className="form-group">
        <label className="text-muted">Название</label>
        <input
          onChange={this.handleChange("title")}
          type="text"
          className="form-control"
          value={title}
        />
      </div>

      <div className="form-group">
        <label className="text-muted">Текст</label>
        <JoditEditor
          editorRef={this.setRef}
          value={this.state.body}
          config={this.config}
          onChange={this.updateContent}
        />
      </div>

      <button
        onClick={this.clickSubmit}
        className="btn btn-block btn-outline-primary btn-sm"
      >
        Послать
      </button>
    </form>
  );

  render() {
    const {
      id,
      post_id,
      title,
      body,
      redirectToProfile,
      error,
      loading
    } = this.state;
    const IsAuthenticated = isAuthenticated()
    if (redirectToProfile) {
      return <Redirect to={`/user/${IsAuthenticated.user._id}`} />;
    }

    return (
      <div className="container">
        <h2 className="mt-5 mb-5">{title}</h2>

        <div
          className="alert alert-danger"
          style={{ display: error ? "" : "none" }}
        >
          {error}
        </div>

        {loading ? (
          <div className="jumbotron text-center">
            <h2>Loading...</h2>
          </div>
        ) : (
            ""
          )}

        <img
          style={{ height: "200px", width: "auto" }}
          className="img-thumbnail"
          src={`${
            process.env.REACT_APP_API_URL
            }/post/photo/${post_id}`}
          onError={img => (img.target.src = `${DefaultPost}`)}
          alt={title}
        />

        {IsAuthenticated.user.role === "admin" &&
          this.editPostForm(title, body)}

        {IsAuthenticated.user._id === id && IsAuthenticated.user.role !== "admin" &&
          this.editPostForm(title, body)}
      </div>
    );
  }
}

export default EditPost;
