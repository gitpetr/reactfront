import React, { Component } from "react";
import { signup } from "../../auth";
import { Link } from "react-router-dom";

class Signup extends Component {
  constructor() {
    super();
    this.state = {
      name: "",
      email: "",
      password: "",
      password2: "",
      error: "",
      confirm: false,
      open: false,
      recaptcha: false
    };
  }

  handleChange = name => event => {
    this.setState({ error: "" });
    this.setState({ [name]: event.target.value });
  };

  confirmPassword = (password, password2) => {
    const confirm = password === password2;

    if (confirm) {
      this.setState({ error: "", confirm });
      return true
    } else {
      this.setState({ error: "Пароли не совпадают" });
      return false
    }
  }

  recaptchaHandler = e => {
    this.setState({ error: "" });
    let userDay = e.target.value.toLowerCase();
    let dayCount;

    if (userDay === "sunday") {
      dayCount = 0;
    } else if (userDay === "monday") {
      dayCount = 1;
    } else if (userDay === "tuesday") {
      dayCount = 2;
    } else if (userDay === "wednesday") {
      dayCount = 3;
    } else if (userDay === "thursday") {
      dayCount = 4;
    } else if (userDay === "friday") {
      dayCount = 5;
    } else if (userDay === "saturday") {
      dayCount = 6;
    }

    if (dayCount === new Date().getDay()) {
      this.setState({ recaptcha: true });
      return true;
    } else {
      this.setState({
        recaptcha: false
      });
      return false;
    }
  };

  clickSubmit = event => {
    event.preventDefault();
    const { name, email, password, password2 } = this.state;
    const user = {
      name,
      email,
      password
    };

    if (!this.confirmPassword(password, password2)) return

    if (this.state.recaptcha) {
      signup(user).then(data => {
        if (data.error) this.setState({ error: data.error });
        else
          this.setState({
            error: "",
            name: "",
            email: "",
            password: "",
            password2: "",
            open: true
          });
      });
    } else {
      this.setState({
        error: "Какой сегодня день недели? Напишите название дня недели по-английски."
      });
    }
  };

  signupForm = (name, email, password, password2, recaptcha) => (
    <form>
      <div className="form-group">
        <label className="text-muted">Название компании</label>
        <input
          onChange={this.handleChange("name")}
          type="text"
          className="form-control"
          value={name}
        />
      </div>
      <div className="form-group">
        <label className="text-muted">Email</label>
        <input
          onChange={this.handleChange("email")}
          type="email"
          className="form-control"
          value={email}
        />
      </div>
      <div className="form-group">
        <label className="text-muted">Пароль</label>
        <input
          onChange={this.handleChange("password")}
          type="password"
          className="form-control"
          value={password}
        />
      </div>

      <div className="form-group">
        <label className="text-muted">Подтверждение пароля</label>
        <input
          onChange={this.handleChange("password2")}
          type="password"
          className="form-control"
        />
      </div>

      <div className="form-group">
        <label className="text-muted">
          {recaptcha ? "Угадали!" : "Название дня недели по-английски?"}
        </label>

        <input
          onChange={this.recaptchaHandler}
          type="text"
          className="form-control"
        />
      </div>

      <button
        onClick={this.clickSubmit}
        className="btn btn-sm btn-block btn-outline-primary"
      >
        Регистрация
      </button>
    </form>
  );

  render() {
    const { name, email, password, error, open, recaptcha } = this.state;
    return (
      <div className="container">
        <h2 className="mt-5 mb-5">Регистрация</h2>

        <hr />
        <br />

        <div
          className="alert alert-danger"
          style={{ display: error ? "" : "none" }}
        >
          {error}
        </div>

        <div
          className="alert alert-info"
          style={{ display: open ? "" : "none" }}
        >
          Новый аккаунт успешно создан. Можете {" "}
          <Link to="/signin">Войти</Link>.
                </div>

        {this.signupForm(name, email, password, recaptcha)}
      </div>
    );
  }
}

export default Signup;
