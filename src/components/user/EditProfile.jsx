import React, { Component } from "react";
import { isAuthenticated } from "../../auth";
import { read, update, updateUser } from "../../services/apiUser";
import { Redirect } from "react-router-dom";
import DefaultProfile from "../../images/avatar.jpg";
import 'jodit';
import 'jodit/build/jodit.min.css';
import JoditEditor from "jodit-react";

class EditProfile extends Component {
  constructor() {
    super();
    this.state = {
      id: "",
      name: "",
      email: "",
      password: "",
      password2: "",
      confirm: false,
      redirectToProfile: false,
      error: "",
      fileSize: 0,
      loading: false,
      about: ""
    };
  }

  init = userId => {
    const token = isAuthenticated().token;
    read(userId, token).then(data => {
      if (data.error) {
        this.setState({ redirectToProfile: true });
      } else {
        this.setState({
          id: data._id,
          name: data.name,
          email: data.email,
          error: "",
          about: data.about
        });
      }
    });
  };

  componentDidMount() {
    this.userData = new FormData();
    const userId = this.props.match.params.userId;
    this.init(userId);
  }

  jodit;
  setRef = jodit => this.jodit = jodit;

  config = {
    readonly: false
  }

  isValid = () => {
    const { name, email, password, password2, fileSize } = this.state;

    if (!this.confirmPassword(password, password2)) return false;

    if (fileSize > 1000000) {
      this.setState({
        error: "Размер картинки должен быть ментьше 1000kb",
        loading: false
      });
      return false;
    }
    if (name.length === 0) {
      this.setState({ error: "Название компании - это обязательное поле", loading: false });
      return false;
    }
    if (!/^\w+([\.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
      this.setState({
        error: "Укажите правильный email",
        loading: false
      });
      return false;
    }
    if (password.length >= 1 && password.length <= 5) {
      this.setState({
        error: "Пароль должен быть не менее 6 символов",
        loading: false
      });
      return false;
    }
    return true;
  };

  handleChange = name => event => {
    this.setState({ error: "" });
    const value =
      name === "photo" ? event.target.files[0] : event.target.value;

    const fileSize = name === "photo" ? event.target.files[0].size : 0;
    this.userData.set(name, value);
    this.setState({ [name]: value, fileSize });
  };

  confirmPassword = (password, password2) => {
    const confirm = password === password2;

    if (confirm) {
      this.setState({ error: "", confirm });
      return true
    } else {
      this.setState({ error: "Пароли не совпадают", loading: false });
      return false
    }
  }

  updateContent = (value) => {
    this.userData.set("about", value);
    this.setState({ about: value })
  }

  clickSubmit = event => {
    event.preventDefault();
    this.setState({ loading: true });

    if (this.isValid()) {
      const userId = this.props.match.params.userId;
      const token = isAuthenticated().token;

      update(userId, token, this.userData).then(data => {
        if (data.error) {
          this.setState({ error: data.error });
        } else if (isAuthenticated().user.role === "admin") {
          this.setState({
            redirectToProfile: true
          });
        } else {
          updateUser(data, () => {
            this.setState({
              redirectToProfile: true
            });
          });
        }
      });
    }
  };

  signupForm = (name, email, password, about) => (
    <form>
      <div className="form-group">
        <label className="text-muted mt-4 mb-0">Загрузите фото компании</label>
        <input
          onChange={this.handleChange("photo")}
          type="file"
          accept="image/*"
          className="form-control mt-0"
        />
      </div>
      <div className="form-group">
        <label className="text-muted">Название компании</label>
        <input
          onChange={this.handleChange("name")}
          type="text"
          className="form-control"
          value={name}
        />
      </div>
      <div className="form-group">
        <label className="text-muted">Email</label>
        <input
          onChange={this.handleChange("email")}
          type="email"
          className="form-control"
          value={email}
        />
      </div>

      <div className="form-group">
        <label className="text-muted">О компании (<small>не забудьте указать вакансии, требование к кандидатам, контакы</small>)</label>

        <JoditEditor
          editorRef={this.setRef}
          value={this.state.about}
          config={this.config}
          onChange={this.updateContent}
        />
      </div>

      <div className="form-group">
        <label className="text-muted">Пароль</label>
        <input
          onChange={this.handleChange("password")}
          type="password"
          className="form-control"
          value={password}
        />
      </div>

      <div className="form-group">
        <label className="text-muted">Подтверждение пароля</label>
        <input
          onChange={this.handleChange("password2")}
          type="password"
          className="form-control"
        />
      </div>

      <button
        onClick={this.clickSubmit}
        className="btn btn-sm btn-block btn-outline-primary"
      >
        Послать
            </button>
    </form>
  );

  render() {
    const {
      id,
      name,
      email,
      password,
      redirectToProfile,
      error,
      loading,
      about
    } = this.state;

    if (redirectToProfile) {
      return <Redirect to={`/user/${id}`} />;
    }

    const photoUrl = id
      ? `${
      process.env.REACT_APP_API_URL
      }/user/photo/${id}?${new Date().getTime()}`
      : DefaultProfile;

    return (
      <div className="container">
        <h2 className="mt-5 mb-5">Редактировать профиль</h2>
        <div
          className="alert alert-danger"
          style={{ display: error ? "" : "none" }}
        >
          {error}
        </div>

        {loading ? (
          <div className="jumbotron text-center">
            <h2>Loading...</h2>
          </div>
        ) : (
            ""
          )}

        <img
          style={{ height: "200px", width: "auto" }}
          className="img-thumbnail"
          src={photoUrl}
          onError={i => (i.target.src = `${DefaultProfile}`)}
          alt={name}
        />

        {isAuthenticated().user.role === "admin" &&
          this.signupForm(name, email, password, about)}

        {isAuthenticated().user._id === id &&
          this.signupForm(name, email, password, about)}
      </div>
    );
  }
}

export default EditProfile;
