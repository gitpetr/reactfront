 import React, { Component } from 'react';
import { isAuthenticated } from "../../auth";
import { Redirect, Link } from "react-router-dom";
import { read } from "../../services/apiUser";
import DefaultProfile from "../../images/avatar.jpg";
import DeleteUser from "./DeleteUser";
const ReactSafeHtml = require('react-safe-html');

class Profile extends Component {
  constructor() {
    super();
    this.state = {
      user: {},
      redirectToSignin: false,
      error: ""
    };
  }

  init = userId => {
    read(userId).then(data => {
      if (data.error) {
        this.setState({ error: data.error });
      } else {
        this.setState({ user: data });
      }
    });
  };

  componentDidMount() {
    const userId = this.props.match.params.userId;
    this.init(userId);
  }

  componentWillReceiveProps(props) {
    const userId = props.match.params.userId;
    this.init(userId);
  }

  render() {
    const updated = new Date(this.state.user.updatedAt)
    const { redirectToSignin, user } = this.state;
    if (redirectToSignin) return <Redirect to="/signin" />;

    const photoUrl = user._id
      ? `${process.env.REACT_APP_API_URL}/user/photo/${user._id}`
      : DefaultProfile;

    return (
      <div className="container">
        <h1 className="mt-5 mb-5">Компания {user.name}</h1>
        <div className="row">
          <div className="col-md-4">
            <img
              style={{ height: "200px", width: "auto" }}
              className="img-thumbnail"
              src={photoUrl}
              onError={i => (i.target.src = `${DefaultProfile}`)}
              alt={user.name}
            />
          </div>

          <div className="col-md-8">
            <div className="lead mt-2">
              <p>{`Обновлено ${new Date(updated)
                .toLocaleString("ru", { day: 'numeric', month: 'long', year: 'numeric' })}`}</p>
                            <hr />
              <div className="lead">{user.about && <ReactSafeHtml html={user.about} />}</div>
            </div>

            <div className="d-block">
              <Link
                className="btn btn-sm btn-block btn-outline-info"
                to={`/post/byuser/${user._id}`}
              >
                Публикации
                </Link>

            </div>

            {isAuthenticated().user &&
              isAuthenticated().user._id === user._id && (
                <>
                <div className="d-block">
                  <Link
                    className="btn btn-sm btn-block btn-outline-success"
                    to={`/user/edit/${user._id}`}
                  >
                    Редактировать профиль
                  </Link>
                </div>
                <DeleteUser userId={user._id} />
              </>
              )
            }

            <div>
              {isAuthenticated().user &&
                isAuthenticated().user.role === "admin" && (
                  <div class="card mt-5">
                    <div className="card-body">
                      <h5 className="card-title">
                        Admin
                                          </h5>
                      <p className="mb-2 text-danger">
                        Edit/Delete as an Admin
                      </p>
                      <Link
                      className="btn btn-sm btn-block btn-outline-success"
                        to={`/user/edit/${user._id}`}
                      >
                        Edit Profile
                      </Link>
                      <DeleteUser userId={user._id} />
                    </div>
                  </div>
                )}
            </div>
          </div>
        </div>

      </div>
    );
  }
}

export default Profile
