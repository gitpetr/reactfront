import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { isAuthenticated } from "../../auth";
import { remove } from "../../services/apiUser";
import { signout } from "../../auth";

class DeleteUser extends Component {
  state = {
    redirect: false
  };

  deleteAccount = () => {
    const token = isAuthenticated().token;
    const authenticateId = isAuthenticated().user._id

    const userId = this.props.userId;
    remove(userId, token).then(data => {
      if (data.error) {
        console.log(data.error);
      } else {
        if (userId === authenticateId) {
          signout(() => console.log("User is deleted"));
        }
        this.setState({ redirect: true });
      }
    });
  };

  deleteConfirmed = () => {
    let answer = window.confirm(
      "Вы действительно хотите удалить свой аккаунт?"
    );
    if (answer) {
      this.deleteAccount();
    }
  };

  render() {
    if (this.state.redirect) {
      return <Redirect to="/" />;
    }
    return (
      <div className="d-block">
        <button
          onClick={this.deleteConfirmed}
          className="btn btn-sm btn-block btn-outline-danger"
        >
          Удалить аккаунт
        </button>
      </div>

    );
  }
}

export default DeleteUser;
